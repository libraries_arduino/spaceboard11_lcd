/*
 Name:		SB11_LCD.h
 Created:	10/15/2019 4:11:51 PM
 Author:	gwatb
 Editor:	http://www.visualmicro.com
*/

#ifndef _SB11_LCD_h
#define _SB11_LCD_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif


#include "LCD20x4.h"

extern LCD20x4 LCD;
extern uint16_t LastKeyChange;

extern void SB11_LCD_init(bool Cursor_ON, bool Cursor_Blink);
#endif
