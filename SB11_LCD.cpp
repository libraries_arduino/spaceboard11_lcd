/*
 Name:		SB11_LCD.cpp
 Created:	10/15/2019 4:11:51 PM
 Author:	gwatb
 Editor:	http://www.visualmicro.com
*/

#include "SB11_initial.h"
#include "SB11_LCD.h"
//#include "LCD20x4.h"

/********************************************************************************************/
/*Function Declare*/
uint16_t get_key(void);
void swCallback(void);
void SB11_LCD_init(bool Cursor_ON, bool Cursor_Blink);
/********************************************************************************************/
/*Global Parameter*/
LCD20x4 LCD(BitMode_4);
uint16_t LastKeyChange = 0;
/********************************************************************************************/

uint16_t get_key(void) {
	uint16_t result = 0;
	result = XgetLastInterruptPin(PIN_LCD_INT);
	result = result | (XgetLastInterruptPinValue(result & 0xFF)<<8) ;
	//return 0xVVPP - VV = value,PP = Pin
	return result;
}

void swCallback(void) {
	LastKeyChange = get_key();
}

void SB11_LCD_init(bool Cursor_ON, bool Cursor_Blink) {
	XpinMode(PIN_LCD_RST , OUTPUT);		//Pin B_7
	XpinMode(PIN_LCD_LED0, OUTPUT);		//Pin A_0
	XpinMode(PIN_LCD_LED1, OUTPUT);		//Pin A_1
	XpinMode(PIN_LCD_LED2, OUTPUT);		//Pin A_2
	XpinMode(PIN_LCD_UP  , INPUT);		//Pin A_3
	XpinMode(PIN_LCD_DW  , INPUT);		//Pin A_4
	XpinMode(PIN_LCD_LT  , INPUT);		//Pin A_5
	XpinMode(PIN_LCD_RT  , INPUT);		//Pin A_6
	XpinMode(PIN_LCD_OK  , INPUT);		//Pin A_7
	XpinMode(PIN_LCD_RS  , OUTPUT);		//Pin B_0
	XpinMode(PIN_LCD_RW  , OUTPUT);		//Pin B_1
	XpinMode(PIN_LCD_EN  , OUTPUT);		//Pin B_2
	XpinMode(PIN_LCD_BL  , OUTPUT);		//Pin B_3
	XpinMode(PIN_LCD_D4  , OUTPUT);		//Pin B_4
	XpinMode(PIN_LCD_D5  , OUTPUT);		//Pin B_5
	XpinMode(PIN_LCD_D6  , OUTPUT);		//Pin B_6
	XpinMode(PIN_LCD_D7  , OUTPUT);		//Pin B_7

	XpullUp(PIN_LCD_RST ,HIGH);
	XpullUp(PIN_LCD_LED0,HIGH);
	XpullUp(PIN_LCD_LED1,HIGH);
	XpullUp(PIN_LCD_LED2,HIGH);
	XpullUp(PIN_LCD_RS  ,HIGH);
	XpullUp(PIN_LCD_RW  ,HIGH);
	XpullUp(PIN_LCD_EN  ,HIGH);
	XpullUp(PIN_LCD_BL  ,HIGH);
	XpullUp(PIN_LCD_D4  ,HIGH);
	XpullUp(PIN_LCD_D5  ,HIGH);
	XpullUp(PIN_LCD_D6  ,HIGH);
	XpullUp(PIN_LCD_D7  ,HIGH);

	XdigitalWrite(PIN_LCD_RST , HIGH);
	XdigitalWrite(PIN_LCD_LED0, HIGH);
	XdigitalWrite(PIN_LCD_LED1, HIGH);
	XdigitalWrite(PIN_LCD_LED2, HIGH);
	XdigitalWrite(PIN_LCD_RS  , HIGH);
	XdigitalWrite(PIN_LCD_RW  , HIGH);
	XdigitalWrite(PIN_LCD_EN  , HIGH);
	XdigitalWrite(PIN_LCD_BL  , HIGH);
	XdigitalWrite(PIN_LCD_D4  , HIGH);
	XdigitalWrite(PIN_LCD_D5  , HIGH);
	XdigitalWrite(PIN_LCD_D6  , HIGH);
	XdigitalWrite(PIN_LCD_D7  , HIGH);

	XattachInterrupt(PIN_LCD_OK, PIN_LCD_INT, swCallback, CHANGE);
	XattachInterrupt(PIN_LCD_UP, PIN_LCD_INT, swCallback, CHANGE);
	XattachInterrupt(PIN_LCD_DW, PIN_LCD_INT, swCallback, CHANGE);
	XattachInterrupt(PIN_LCD_LT, PIN_LCD_INT, swCallback, CHANGE);
	XattachInterrupt(PIN_LCD_RT, PIN_LCD_INT, swCallback, CHANGE);


	LCD.RS_pin = PIN_LCD_RS;
	LCD.RW_pin = PIN_LCD_RW;
	LCD.EN_pin = PIN_LCD_EN;
	LCD.BL_pin = PIN_LCD_BL;
	LCD.D4_pin = PIN_LCD_D4;

	LCD.pinIn  = XdigitalRead;
	LCD.pinOut = XdigitalWrite;
	LCD.portIn = XportRead;
	LCD.portOut= XportWrite;

	LCD.begin(true, true);
}
